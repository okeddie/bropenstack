#!/usr/bin/bash
#
# Usage:
# ansible nova-controllers:nova-computes -cssh -m package -a "name=lldpad state=latest"
# ansible nova-controllers:nova-computes -cssh -m service -a "name=lldpad state=started enabled=true"
# ansible nova-controllers:nova-computes -cssh -m script -a "files/lldpscript.sh"
#
for i in eth0 eth1 ;
do echo "enabling lldp for interface: $i" ;
lldptool set-lldp -i $i adminStatus=rxtx ;
lldptool -T -i $i -V sysName enableTx=yes;
lldptool -T -i $i -V portDesc enableTx=yes ;
lldptool -T -i $i -V sysDesc enableTx=yes;
lldptool -T -i $i -V sysCap enableTx=yes;
lldptool -T -i $i -V mngAddr enableTx=yes;
done
