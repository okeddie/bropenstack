# RHEL/CentOS example
```
[root@golgotha ~]# cat /etc/httpd/conf.d/pxeboot.conf
Alias /centos7 /var/lib/tftpboot/centos7

<Directory /var/lib/tftpboot/centos7>
Options Indexes FollowSymLinks
Order Deny,Allow
Deny from all
Allow from 127.0.0.1 192.168.0.0/24
</Directory>


[root@golgotha ~]# ll /var/lib/tftpboot/
total 4326936
-rw-r--r--. 1 root  root         439 Mar 18  2017 altmbr.bin
-rw-r--r--. 1 root  root         439 Mar 18  2017 altmbr_c.bin
-rw-r--r--. 1 root  root         439 Mar 18  2017 altmbr_f.bin
-r--r--r--. 1 eddie eddie       2048 Dec  5  2016 boot.cat
-rw-r--r--. 1 eddie eddie         84 Dec  5  2016 boot.msg
-rw-r--r--. 1 root  root        1620 Mar 18  2017 cat.c32
drwxr-xr-x. 8 root  root        4096 Aug  1 23:40 centos7
-rw-rw-r--. 1 eddie eddie 4379901952 Mar 17  2017 CentOS-7-x86_64-DVD-1611.iso
-rw-r--r--. 1 root  root       25300 Mar 18  2017 chain.c32
-rw-r--r--. 1 root  root        1256 Mar 18  2017 cmd.c32
-rw-r--r--. 1 root  root        3688 Mar 18  2017 cmenu.c32
-rw-r--r--. 1 root  root        1492 Mar 18  2017 config.c32
-rw-r--r--. 1 root  root        4216 Mar 18  2017 cptime.c32
-rw-r--r--. 1 root  root        4508 Mar 18  2017 cpu.c32
-rw-r--r--. 1 root  root        1776 Mar 18  2017 cpuid.c32
-rw-r--r--. 1 root  root        2772 Mar 18  2017 cpuidtest.c32
-rw-r--r--. 1 root  root        1592 Mar 18  2017 debug.c32
-rw-r--r--. 1 root  root        4076 Mar 18  2017 dhcp.c32
drwxr-xr-x. 2 root  root        4096 Mar 18  2017 diag
-rw-r--r--. 1 root  root        2984 Mar 18  2017 dir.c32
-rw-r--r--. 1 root  root        2016 Mar 18  2017 disk.c32
-rw-r--r--. 1 root  root        8636 Mar 18  2017 dmi.c32
-rw-r--r--. 1 root  root       11956 Mar 18  2017 dmitest.c32
drwxr-xr-x. 2 root  root        4096 Mar 18  2017 dosutil
-rw-r--r--. 1 root  root        3264 Mar 18  2017 elf.c32
-rw-r--r--. 1 root  root        2736 Mar 18  2017 ethersel.c32
-rw-r--r--. 1 root  root       10472 Mar 18  2017 gfxboot.c32
-rw-r--r--. 1 root  root         440 Mar 18  2017 gptmbr.bin
-rw-r--r--. 1 root  root         440 Mar 18  2017 gptmbr_c.bin
-rw-r--r--. 1 root  root         440 Mar 18  2017 gptmbr_f.bin
-rw-r--r--. 1 root  root        1604 Mar 18  2017 gpxecmd.c32
-rw-r--r--. 1 eddie eddie        293 Dec  5  2016 grub.conf
-rw-r--r--. 1 root  root      164812 Mar 18  2017 hdt.c32
-rw-r--r--. 1 root  root        3732 Mar 18  2017 hexdump.c32
-rw-r--r--. 1 root  root        1844 Mar 18  2017 host.c32
-rw-r--r--. 1 root  root        1748 Mar 18  2017 ifcpu64.c32
-rw-r--r--. 1 root  root        4136 Mar 18  2017 ifcpu.c32
-rw-r--r--. 1 root  root        4288 Mar 18  2017 ifmemdsk.c32
-rw-r--r--. 1 root  root        1908 Mar 18  2017 ifplop.c32
-rw-r--r--. 1 eddie eddie   43372552 Dec  5  2016 initrd.img
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdpfx.bin
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdpfx_c.bin
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdpfx_f.bin
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdppx.bin
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdppx_c.bin
-rw-r--r--. 1 root  root         432 Mar 18  2017 isohdppx_f.bin
drwxr-xr-x. 2 eddie eddie       4096 Jul  6 00:52 isolinux
-rw-r--r--. 1 eddie eddie      40960 Mar 18  2017 isolinux.bin
-rw-r--r--. 1 eddie eddie       3068 Dec  5  2016 isolinux.cfg
-rw-r--r--. 1 root  root       40960 Mar 18  2017 isolinux-debug.bin
-rw-r--r--. 1 root  root        1676 Mar 18  2017 kbdmap.c32
-rw-r--r--. 1 root  root        4932 Mar 18  2017 kontron_wdt.c32
-rw-r--r--. 1 root  root      116908 Mar 18  2017 ldlinux.c32
-rw-r--r--. 1 root  root        5100 Mar 18  2017 lfs.c32
-rw-r--r--. 1 root  root      182016 Mar 18  2017 libcom32.c32
-rw-r--r--. 1 root  root       67988 Mar 18  2017 libgpl.c32
-rw-r--r--. 1 root  root      101868 Mar 18  2017 liblua.c32
-rw-r--r--. 1 root  root       24100 Mar 18  2017 libmenu.c32
-rw-r--r--. 1 root  root       23572 Mar 18  2017 libutil.c32
-rw-r--r--. 1 root  root        4724 Mar 18  2017 linux.c32
-rw-r--r--. 1 root  root       75626 Mar 18  2017 lpxelinux.0
-rw-r--r--. 1 root  root        2988 Mar 18  2017 ls.c32
-rw-r--r--. 1 root  root        6828 Mar 18  2017 lua.c32
-rw-r--r--. 1 root  root       11088 Mar 18  2017 mboot.c32
-rw-r--r--. 1 root  root         440 Mar 18  2017 mbr.bin
-rw-r--r--. 1 root  root         440 Mar 18  2017 mbr_c.bin
-rw-r--r--. 1 root  root         440 Mar 18  2017 mbr_f.bin
-rw-r--r--. 1 root  root       25628 Mar 18  2017 memdisk
-rw-r--r--. 1 root  root        2484 Mar 18  2017 meminfo.c32
-rw-r--r--. 1 root  root       26272 Mar 18  2017 menu.c32
-rw-r--r--. 1 root  root        3344 Mar 18  2017 pci.c32
-rw-r--r--. 1 root  root        3480 Mar 18  2017 pcitest.c32
-rw-r--r--. 1 root  root        2988 Mar 18  2017 pmload.c32
-rw-r--r--. 1 root  root        1668 Mar 18  2017 poweroff.c32
-rw-r--r--. 1 root  root        3132 Mar 18  2017 prdhcp.c32
-rw-r--r--. 1 root  root        1504 Mar 18  2017 pwd.c32
-rw-r--r--. 1 root  root       12400 Mar 18  2017 pxechn.c32
-rw-r--r--. 1 root  root       43297 Mar 18  2017 pxelinux.0
drwxr-xr-x. 2 root  root        4096 Jul  6 00:55 pxelinux.cfg
-rw-r--r--. 1 root  root         113 Mar 18  2017 pxelinux.cfg.old
-rw-r--r--. 1 root  root        1376 Mar 18  2017 reboot.c32
-rw-r--r--. 1 root  root       13820 Mar 18  2017 rosh.c32
-rw-r--r--. 1 root  root        1636 Mar 18  2017 sanboot.c32
-rw-r--r--. 1 root  root        3176 Mar 18  2017 sdi.c32
-rw-r--r--. 1 eddie eddie        186 Sep 30  2015 splash.png
-rw-r--r--. 1 root  root       14780 Mar 18  2017 sysdump.c32
-rw-r--r--. 1 root  root      236544 Mar 18  2017 syslinux64.exe
-rw-r--r--. 1 root  root        8864 Mar 18  2017 syslinux.c32
-rw-r--r--. 1 root  root      194196 Mar 18  2017 syslinux.com
-rw-r--r--. 1 root  root      229888 Mar 18  2017 syslinux.exe
-r--r--r--. 1 eddie eddie       2215 Dec  5  2016 TRANS.TBL
-rw-r--r--. 1 root  root        2964 Mar 18  2017 vesa.c32
-rw-r--r--. 1 root  root        2204 Mar 18  2017 vesainfo.c32
-rw-r--r--. 1 eddie eddie      26788 Mar 18  2017 vesamenu.c32
-rwxr-xr-x. 1 root  root     5392080 Nov 22  2016 vmlinuz
-rw-r--r--. 1 root  root        1868 Mar 18  2017 vpdtest.c32
-rw-r--r--. 1 root  root        2488 Mar 18  2017 whichsys.c32
-rw-r--r--. 1 root  root        3556 Mar 18  2017 zzjson.c32


[root@golgotha ~]# cat /var/lib/tftpboot/pxelinux.cfg/default
default menu.c32
prompt 0
timeout 300
ONTIMEOUT local
menu title ########## PXE Boot Menu ##########
label 1
menu label ^1) Install CentOS 7 x64 with Local Repo
kernel centos7/images/pxeboot/vmlinuz
append initrd=centos7/images/pxeboot/initrd.img method=http://192.168.0.3/centos7 devfs=nomount net.ifnames=0 biosdevname=0 hpsa.hpsa_allow_any=1 hpsa.hpsa_simple_mode=1 ks=http://192.168.0.3/centos7/hpdl380-g5.cfg

label 2
menu label ^2) Boot from local drive localboot




[root@golgotha ~]# grep -A2 manage-dhcp /home/eddie/.bashrc
function manage-dhcpd () {
ACTION=$1
sudo systemctl $ACTION tftp.service dhcpd.service httpd.service

yum install dnsmasq
yum install syslinux
yum install tftp-server
cp -r /usr/share/syslinux/* /var/lib/tftpboot



[root@golgotha ~]# cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
allow booting; allow bootp; ddns-update-style interim; ignore client-updates; subnet 192.168.0.0 netmask 255.255.255.0 { option subnet-mask 255.255.255.0; option broadcast-address 192.168.0.255; range dynamic-bootp 192.168.0.190 192.168.0.199; next-server 192.168.0.3; filename "pxelinux.0"; }

[root@golgotha ~]# ll /var/lib/tftpboot/centos7/
total 47964
-rwxr-xr-x. 1 root root       14 May 20  2017 CentOS_BuildTag
drwxr-xr-x. 3 root root     4096 May 20  2017 EFI
-rwxr-xr-x. 1 root root      215 May 20  2017 EULA
-rwxr-xr-x. 1 root root    18009 May 20  2017 GPL
-rw-r--r--. 1 root root     1631 Aug  1 23:40 hpdl380-g5.cfg
-rw-r--r--. 1 root root     1740 Jul  6 00:41 hpdl380-g5.cfg.old
drwxr-xr-x. 3 root root     4096 May 20  2017 images
-rwxr-xr-x. 1 root root 43372552 Mar 18  2017 initrd.img
drwxr-xr-x. 2 root root     4096 Jul 14 02:45 isolinux
drwxr-xr-x. 2 root root     4096 May 20  2017 LiveOS
drwxr-xr-x. 2 root root   274432 May 20  2017 Packages
drwxr-xr-x. 2 root root     4096 May 20  2017 repodata
-rwxr-xr-x. 1 root root     1690 May 20  2017 RPM-GPG-KEY-CentOS-7
-rwxr-xr-x. 1 root root     1690 May 20  2017 RPM-GPG-KEY-CentOS-Testing-7
-rwxr-xr-x. 1 root root     2883 May 20  2017 TRANS.TBL
-rwxr-xr-x. 1 root root  5392080 Mar 18  2017 vmlinuz
[root@golgotha ~]#


[eddie@golgotha ~]$ cat /var/lib/tftpboot/centos7/hpdl380-g5.cfg
#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use network installation
url --url="http://192.168.0.3/centos7"
# Use graphical install
graphical
reboot
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=eth0 --ipv6=auto --no-activate
network  --bootproto=static --device=eth1 --gateway=192.168.0.1 --ip=192.168.0.42 --nameserver=192.168.0.1 --netmask=255.255.255.0 --ipv6=auto --activate
network  --hostname=localhost.localdomain

# Root password
rootpw --iscrypted encrypted_stuff
# System services
services --enabled="chronyd"
# System timezone
timezone America/New_York --isUtc
user --groups=wheel --name=janky_user --password=encrypted_stuff --iscrypted --gecos="janky_user"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda

%packages
@^minimal
@core
chrony
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy luks --minlen=6 --minquality=50 --notstrict --nochanges --notempty
%end

```

# Debian/Ubuntu Version
* Debian WIP, probably don't use these instructions yet. idk where i got the kernel file from.
```
# start iso download and work on pxelinux config while image downloads.

# modify pxelinux config
[root@fedora01 ~]# tail -n4 /var/lib/tftpboot/pxelinux.cfg/default 
label 4
menu label ^4) Debian 10
kernel debian10/debian-installer/amd64/linux
append vga=788 initrd=debian10/debian-installer/amd64/initrd.gz --- quiet biosdevname=0 net.ifnames=0

# ensure the directory structure exists, once image is downloaded and mounted
# then you can actually copy the files from the image from debian-installer:
mkdir -p /var/lib/tftpboot/debian10/debian-installer/amd64/

# copy the kernel file there:

# you'll need to mount it to get the initrd files off and copied into tftp directory.
[root@fedora01 ~]# vim /etc/fstab 
[root@fedora01 ~]# tail -n1 !$
tail -n1 /etc/fstab
/isos/debian-10.0.0-amd64-netinst.iso /var/www/html/debian-10.0.0-amd64-netinst.iso/iso iso9660 ro,auto 0 0

# make dir
[root@fedora01 ~]# mkdir -p /var/www/html/debian-10.0.0-amd64-netinst.iso/iso

# mount
[root@fedora01 ~]# mount /var/www/html/debian-10.0.0-amd64-netinst.iso/iso

[root@fedora01 ~]# ll /var/www/html/debian-10.0.0-amd64-netinst.iso/iso/

```

* ubuntu
```
# mount iso
[root@fedora01 ~]# grep ubuntu20 /etc/fstab 
#/isos/ubuntu-20.04-live-server-amd64.iso /var/www/html/ubuntu20.04/iso iso9660 ro,auto 0 0


# ensure directoy exists for tftp
mkdir -p /var/lib/tftpboot/ubuntu20.04/

# copy boot files
[root@fedora01 ~]# find /var/www/html/ubuntu20.04/iso/ -name vmlinuz
/var/www/html/ubuntu20.04/iso/casper/vmlinuz
[root@fedora01 ~]# find /var/www/html/ubuntu20.04/iso/ -name initrd
/var/www/html/ubuntu20.04/iso/casper/initrd
[root@fedora01 ~]# cp -a /var/www/html/ubuntu20.04/iso/casper/vmlinuz /var/lib/tftpboot/ubuntu20.04/vmlinuz 
cp: overwrite '/var/lib/tftpboot/ubuntu20.04/vmlinuz'? y
[root@fedora01 ~]# diff /var/www/html/ubuntu20.04/iso/casper/vmlinuz /var/lib/tftpboot/ubuntu20.04/vmlinuz 
[root@fedora01 ~]# 
[root@fedora01 ~]# cp -a /var/www/html/ubuntu20.04/iso/casper/initrd /var/lib/tftpboot/ubuntu20.04/initrd 
cp: overwrite '/var/lib/tftpboot/ubuntu20.04/initrd'? y


# add entry to pxelinuxconf
[root@fedora01 ~]# vim /var/lib/tftpboot/pxelinux.cfg/default 
[root@fedora01 ~]# tail -n5 /var/lib/tftpboot/pxelinux.cfg/default
label 5
menu label ^5) Ubuntu 20.04
kernel ubuntu20.04/vmlinuz
append vga=788 root=/dev/ram0 ramdisk_size=1500000 ip=dhcp initrd=ubuntu20.04/initrd biosdevname=0 net.ifnames=0 url=http://192.168.0.3/ubuntu20.04/ubuntu-20.04.2-live-server-amd64.iso

# finally ensure apache can serve the directory:
[root@fedora01 ~]# tail -n6 /etc/httpd/conf.d/pxeboot.conf
<Directory /var/www/html/ubuntu20.04>
Options Indexes FollowSymLinks
Order Deny,Allow
Deny from all
Allow from 127.0.0.1 192.168.0.0/24
</Directory>


# vim /etc/httpd/conf/httpd.conf
<Directory "/var/www/html">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

# (note this is an edit after ubuntu 20.04, we're now doing 22.04 here):
# Ensure you symlink the path referenced in the pxelinux.cfg
[root@fedora01 ~]# ln -s /isos/ubuntu-22.04-live-server-amd64.iso /var/www/html/ubuntu22.04/ubuntu-22.04-live-server-amd64.iso

reload apache and browse to:
http://192.168.0.3/ubuntu20.04/iso/



## Example2 using debian11 live install
# create directory struct
mkdir /var/lib/tftpboot/debian11 /var/www/html/debian11/iso
# make mount
/isos/debian-live-11.0.0-amd64-kde.iso /var/www/html/debian11/iso iso9660 ro,auto 0 0

# copy initrd file
[root@fedora01 debian11]# find /var/www/html/debian11/ -name \*initrd.img\*
/var/www/html/debian11/iso/live/initrd.img-5.10.0-8-amd64
[root@fedora01 debian11]# cp -a /var/www/html/debian11/iso/live/initrd.img-5.10.0-8-amd64 /var/lib/tftpboot/debian11/

# copy live/vmlinuz file
[root@fedora01 debian11]# find /var/www/html/debian11/ -iname \*vmlinuz\*
/var/www/html/debian11/iso/d-i/gtk/vmlinuz
/var/www/html/debian11/iso/d-i/vmlinuz
/var/www/html/debian11/iso/live/vmlinuz-5.10.0-8-amd64

# update all the kernel params in pxe config
label 6
menu label ^6) Debian 11
kernel debian11/vmlinuz-5.10.0-8-amd64
append vga=788 initrd=debian11/initrd.img-5.10.0-8-amd64 dhcp ethdevice=eth0 locales=en_US.UTF-8 boot=live fetch=http://192.168.0.3/debian11/iso/live/filesystem.squashfs biosdevname=0 net.ifnames=0

# append apache config like previous ones to follow symlinks

```
