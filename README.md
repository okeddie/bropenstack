A collection of scripts for bropenstack deployment.

## Make macaddrs easily portable into ios | I
```
[eddie.vasquez@devops bropenstack]$ ansible nova-computes -m setup -a "filter=ansible_eth0" |grep -i 'success\|macaddress' |sed 's/://;s/://;s/:/./;s/://;s/:/./;s/://'
c-192-168-0-206.c0003.sat.bropenstack.com | SUCCESS => {
            "macaddress" "0008.546e.7755",
c-192-168-0-203.bropenstack.com | SUCCESS => {
            "macaddress" "0018.fe75.24ca",
c-192-168-0-201.bropenstack.com | SUCCESS => {
            "macaddress" "001c.c4ee.f3c4",
c-192-168-0-207.bropenstack.com | SUCCESS => {
            "macaddress" "d4ae.5292.df36",
c-192-168-0-200.bropenstack.com | SUCCESS => {
            "macaddress" "001e.0bc8.794c",
```

# Pre-Req
Ensure DNS exists for nodes and hostname is properly set as nodes are usually referenced by {{ inventory_hostname }}
```
ansible -i bropenstack-inventory.yaml c-192-168-0-220.bropenstack.com -c ssh -m ping --private-key=~/.ssh/bro.key --user root
```
HOW TO: Playbook order
# All nodes:
- adjust-node-networking.yaml : fixes network from base install.
- deploy-baseline.yaml : installs mainly base packages needed for OS operation as well as service cleanup/selinux cleanup.

# Controller nodes:
- [deploy-nova-controller.yaml](deploy-nova-controller.yaml) : deploys nova services and supporting services. (memcached, mariadb, keystone, etc).
  - for ubuntu i had to ensure /etc/keystone/etc/keystone/fernet-keys/ was owned by keystone 
- [deploy-nova-cp.yaml](deploy-nova-cp.yaml) : deploys nova services and related. (nova-api, nova-consoleauth, nova-scheduler, etc).
## run in this sequence to ensure smooth deployment:
### STAGE1 | setup basic authentication mainly for keystone
```
ansible-playbook -i inventory/oscian.yaml deploy-nova-cp.yaml -e "master_pass=XXXXX cell=c0001" -t stage1
```
### STAGE2 | setup databases scaffolding
```
ansible-playbook -i inventory/oscian.yaml deploy-nova-cp.yaml -e "master_pass=XXXXX cell=c0001" -t stage2
```

### STAGE3 | this primes our databases for our next service installs.  
ensure when you run stage3 for the 1st time, you add ```init_cell=true sync_db=true``` so the nova_api role can do the database inits  
```
ansible-playbook -i inventory/oscian.yaml deploy-nova-cp.yaml -e "master_pass=XXXXX cell=c0001 init_cell=true sync_db=true" -t stage3
```
### STAGE4 | This installs our required endpoints
make sure we skip tags for stage 3 because it wants facts and looks for files we don't need at stage4
```
ansible-playbook -i inventory/oscian.yaml deploy-nova-cp.yaml -e "cell=c0001 master_pass=XXXXX" -t stage4 --skip-tags=stage3
```
- [deploy-glance.yaml](deploy-glance.yaml) : deploys glance.
- [deploy-neutron.yaml](deploy-neutron.yaml) : deploys neutron. Note: does not adjust security groups.
- [deploy-horizon.yaml](deploy-horizon.yaml) : deploys horizon dashboard
- [cinder-controller](#cinder-controller)
- [deploy-db-backups.yaml](deploy-db-backups.yaml) : deploys basic database backups, very crudely. *note* change remote host for offsite backup.
- [bootstrap-flavors.yaml](bootstrap-flavors.yaml) : add flavors
- [bootstrap-security-groups.yaml](bootstrap-security-groups.yaml) : add security groups

# Compute nodes:
- deploy-nova-compute.yaml : deploys compute node services including nova-compute and neutron agent configs.
- ansible-playbook -i inventory/bropenstack.yaml -l c-192-168-0-200.c0001.sat.bropenstack.com deploy-node-exporter.yaml

# Octavia:
* [deploy-octavia-controller.yaml](deploy-octavia-controller.yaml) : deploys all the required octavia components. Requires manual upload of public key AS the octavia service user.
* Requires manual upload of public key AS the octavia service user.
* The security group ID should be the security group found AS the octavia service account.
* The image id used and tagged 'amphora' should be the PROJECT ID, of the admin service account unless images are built using another user.
* You might have to virt-customize commands to clean up cloud-init files in the image too.
```bash
#CENTOS DID NOT WORK, USE UBUNTU 18.04 Base image to build octavia image. Then:
apt update
apt -y install python-pip python-virtualenv install virtualenv qemu-utils git kpartx debootstrap
virtualenv octavia_disk_image_create
source octavia_disk_image_create/bin/activate
git clone https://github.com/openstack/octavia.git
cd octavia/diskimage-create
pip install -r requirements.txt
chmod 0644 /boot/vmlinuz*
./diskimage-create.sh -r [ROOT_PASS] -o stable-ubuntu-amphora.qcow2
rsync -av --progress stable-ubuntu-amphora.qcow2 192.168.0.202:/root/amphora_build_2020-12-20

# clean cloud-init on openstack controller after image is created.
dnf install -y libguestfs-tools
export LIBGUESTFS_BACKEND=direct
# make sure root ssh is enabled still:
virt-edit  stable-ubuntu-amphora.qcow2 /etc/cloud/cloud.cfg
virt-customize --format qcow2 -a stable-ubuntu-amphora.qcow2 \
    --delete /etc/cloud/cloud.cfg.d/92-ec2-datasource.cfg

virt-customize --format qcow2 -a stable-ubuntu-amphora.qcow2 \
    --delete /etc/cloud/cloud.cfg.d/91-dib-cloud-init-datasources.cfg

# Create image with tag:
openstack image create --tag amphora --min-disk 5 --min-ram 1024 \
    --public --file stable-ubuntu-amphora.qcow2 --disk-format qcow2 amphora-ubuntu18

# Set a tag if image was made without tag:
openstack image set --tag amphora 24b850a3-089a-474d-8363-fc00888ef70f

# get project id of image owner, usually admin:
openstack project show admin -c id -f value

# Unset a tag if needing to change image:
openstack image unset --tag amphora 24b850a3-089a-474d-8363-fc00888ef70f
```

Create network as it's not in the process flow of the docs:
```bash
openstack network create --share --external \
    --provider-physical-network provider --provider-network-type flat provider

openstack subnet create --network provider \
    --allocation-pool start=192.168.0.230,end=192.168.0.250 \
    --dns-nameserver 192.168.0.1 --gateway 192.168.0.1 \
    --subnet-range 192.168.0.0/24 provider
```
# Cinder controller
- [deploy-cinder-controller.yaml](deploy-cinder-controller.yaml) : deploys cinder controller.
# Adding additional storage nodes:
- ensure you've created the volume group like:
```
2020-01-21 16:31:47.785 3617 ERROR cinder.volume.manager Stderr: u'File descriptor 20 (/dev/urandom) leaked on vgs invocation. Parent PID 3629: /usr/bin/python2\n  Volume group "cinder-volumes" not found\n  Cannot process volume group cinder-volumes\n'
2020-01-21 16:31:47.785 3617 ERROR cinder.volume.manager
2020-01-21 16:31:47.904 3617 INFO cinder.volume.manager [req-638dcd02-f7c3-459b-91fc-75db6a111610 - - - - -] Initializing RPC dependent components of volume driver LVMVolumeDriver (3.0.0)
2020-01-21 16:31:47.905 3617 ERROR cinder.utils [req-638dcd02-f7c3-459b-91fc-75db6a111610 - - - - -] Volume driver LVMVolumeDriver not initialized
2020-01-21 16:31:47.906 3617 ERROR cinder.volume.manager [req-638dcd02-f7c3-459b-91fc-75db6a111610 - - - - -] Cannot complete RPC initialization because driver isn't initialized properly.: DriverNotInitialized: Volume driver not ready.
[root@storage-192-168-0-221 ~]# fdisk /dev/sda
Command (m for help): n
Partition number (3-128, default 3):
First sector (34-7811891166, default 209721344):
Last sector, +sectors or +size{K,M,G,T,P} (209721344-7811891166, default 7811891166):
Created partition 3


Command (m for help): p

Disk /dev/sda: 3999.7 GB, 3999688294400 bytes, 7811891200 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: gpt
Disk identifier: 4CF4683D-B4B7-4298-A472-A1A7F6614BB9


#         Start          End    Size  Type            Name
 1         2048         6143      2M  BIOS boot
 2         6144    209721343    100G  Microsoft basic
 3    209721344   7811891166    3.6T  Linux filesyste

Command (m for help): t
Partition number (1-3, default 3): 8e
Partition number (1-3, default 3): 3
Partition type (type L to list all types): 8e
[root@storage-192-168-0-221 ~]# partprobe
[root@storage-192-168-0-221 ~]# pvcreate /dev/sda3
  Physical volume "/dev/sda3" successfully created.
[root@storage-192-168-0-221 ~]# vgs
[root@storage-192-168-0-221 ~]# vgcreate cinder-volumes /dev/sda3
  Volume group "cinder-volumes" successfully created
```
- deploy-cinder-controller.yaml : deploys cinder controller node which configures easily on same controller as above.
- deploy-cinder-node.yaml : deploys cinder storage node
- deploy-cinder-client-compute.yaml : deploys cinder configuration onto computes


Make a bash alias for basic instance info.
```
alias osl='openstack server list --os-cloud=os-admin --all-projects --long -c ID -c Host -c Name'
```

# vnc notes
* run vnc with:
```
#!/bin/bash

vncserver :1 -geometry 1280x1024

```
* then setup password
```
vncpasswd -user -weakpwd
```

# Ansible Notes:
You will likely want to add config to prevent retry files as well as host key checks:
cat ~/.ansible.cfg
[defaults]
retry_files_enabled = False
host_key_checking = False

Networking Notes:
Nodes will need an un-ip'd bridge interface named eth0 for neutron provider network.
This means on the compute nodes, do the following:
  - add "net.ifnames=0 biosdevname=0" to GRUB_CMDLINE_LINUX in /etc/default/grub
  - make grub config: grub2-mkconfig -o /boot/grub2/grub.cfg
  - configure ifcfg-eth* files as normal.
Important Neutron note! THE CONTROLLER MUST HAVE A BRIDGE TO eth0 layer 2 for dhcp-agent to properly give vms ips.
Else: the vm will stay paused because of "ProcessExecutionError: Exit code: 2; Stdin: ; Stdout: ; Stderr: RTNETLINK answers: Permission denied" in /var/log/neutron/linuxbridge-agent.log


# Setting up HP RAID:
When you boot up and go into the install, press tab to get to your install options. From here append hpsa.hpsa_simple_mode=1 hpsa.hpsa_allow_any=1 to your boot options.

The install should now pick up on your LD and you should be able to install as normal. After you reboot however you’ll have to make this more permanent. At the grub screen press e to get back to your boot options. From here append hpsa.hpsa_simple_mode=1 hpsa.hpsa_allow_any=1 again so that you can get into the machine.

Once you’re logged in open up /etc/default/grub and add these options to the file. The file should now look something like:
```
GRUB_TIMEOUT=5
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="rd.lvm.lv=centos/root rd.lvm.lv=centos/swap crashkernel=auto rhgb quiet hpsa.hpsa_simple_mode=1 hpsa.hpsa_allow_any=1"
GRUB_DISABLE_RECOVERY="true"
Now run grub2-mkconfig -o /boot/grub2/grub.cfg and reboot the machine. It should be detected from now on each time you boot.
```
Setting up a kickstart answer was along the lines of adding an anakonda file to:
http://192.168.0.3/centos7/hpdl380-g5.cfg

then calling it in 3 different files that im not sure which cause proper collection but probably default:
/var/lib/tftpboot/pxelinux.cfg/default
```
[root@golgotha tftpboot]# cat pxelinux.cfg/default
default menu.c32
prompt 0
timeout 300
ONTIMEOUT local
menu title ########## PXE Boot Menu ##########
label 1
menu label ^1) Install CentOS 7 x64 with Local Repo
kernel centos7/images/pxeboot/vmlinuz
append initrd=centos7/images/pxeboot/initrd.img method=http://192.168.0.3/centos7 devfs=nomount net.ifnames=0 biosdevname=0 hpsa.hpsa_allow_any=1 hpsa.hpsa_simple_mode=1 ks=http://192.168.0.3/centos7/hpdl380-g5.cfg

label 2
menu label ^2) Boot from local drive localboot

label 3
menu label ^3) Install Debian 10
kernel debian10/debian-installer/amd64/linux
append vga=788 initrd=debian10/debian-installer/amd64/initrd.gz --- quiet
```

* For Debian10 install, had to:
```
mkdir /var/lib/tftpboot/debian10
cd !$
wget http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar -zxf netboot.tar.gz .
```


# need to setup DHCP server:
```
/etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
allow booting;
allow bootp;
ddns-update-style interim;
ignore client-updates;
subnet 192.168.0.0 netmask 255.255.255.0
{
option subnet-mask 255.255.255.0;
option broadcast-address 192.168.0.255;
range dynamic-bootp 192.168.0.190 192.168.0.199;
next-server 192.168.0.3;
option domain-name-servers 192.168.0.1;
option routers 192.168.0.1;
filename "pxelinux.0";
}
```

but doesn't hurt at least to have it on the images grub:
```
[root@golgotha tftpboot]# cat centos7/isolinux/grub.conf
#debug --graphics
default=1
splashimage=@SPLASHPATH@
timeout 60
hiddenmenu
title Install CentOS Linux 7
        findiso
        kernel @KERNELPATH@ @ROOT@ net.ifnames=0 biosdevname=0 hpsa.hpsa_allow_any=1 hpsa.hpsa_simple_mode=1 ks=http://192.168.0.3/centos7/hpdl380-g5.cfg
        initrd @INITRDPATH@
title Test this media & install CentOS Linux 7
        findiso
        kernel @KERNELPATH@ @ROOT@ rd.live.check quiet
        initrd @INITRDPATH@
```

# Uploading an image and amphora for octavia:
from the octavia repo, find script
```
this probably does not work.
./diskimage-create.sh -i centos -o image.qcow2 -s 3
A known working attempt:
[root@controller-192-168-0-202 ~]# export DIB_RELEASE=bionic
[root@controller-192-168-0-202 ~]# disk-image-create -a amd64 -o ubuntu-18.04.2 vm ubuntu
2019-02-19 01:59:39.850 | diskimage-builder version 2.16.0
2019-02-19 01:59:39.852 | Building elements: base vm ubuntu block-device-mbr
```

also another working attempt:
```bash
export DIB_RELEASE=focal LIBGUESTFS_BACKEND=direct
disk-image-create -o ubu20.04.qcow2 -t qcow2 vm ubuntu
virt-customize --format qcow2 -a ubu20.04.qcow2 --root-password password:$PLAIN_TEXT_PASS -v
Then to fix cloud-init or even if you can before upload, might delete files:
[root@aio-192-168-0-206 ~]# virt-edit ubu18.04.qcow2 /etc/cloud/cloud.cfg
[root@aio-192-168-0-206 ~]# virt-customize --format qcow2 -a ubu18.04.qcow2 \
    --delete /etc/cloud/cloud.cfg.d/92-ec2-datasource.cfg

[   0.0] Examining the guest ...
[   5.6] Setting a random seed
virt-customize: warning: random seed could not be set for this type of
guest
[   5.6] Deleting: /etc/cloud/cloud.cfg.d/92-ec2-datasource.cfg
[   6.0] Finishing off


[root@aio-192-168-0-206 ~]# virt-customize --format qcow2 -a ubu18.04.qcow2 \
    --delete /etc/cloud/cloud.cfg.d/91-dib-cloud-init-datasources.cfg

[   0.0] Examining the guest ...
[   5.6] Setting a random seed
virt-customize: warning: random seed could not be set for this type of
guest
[   5.6] Deleting: /etc/cloud/cloud.cfg.d/91-dib-cloud-init-datasources.cfg
[   6.0] Finishing off
/etc/cloud/cloud.cfg.d/{other files also that interfere with cloud-init from openstack}

# create image process
glance image-create --name 'Ubuntu 20' --visibility public --disk-format qcow2 \
    --min-disk 10 --container-format bare < ubu20.04.qcow2

[root@controller-192-168-0-202 ~]# glance image-create --name 'Ubuntu 18.04.2 Base' \
    --visibility public --disk-format qcow2 --container-format bare < ubuntu-18.04.2.qcow2

# Had to fix the name and disk:
glance image-update --name 'Ubuntu 18.04.2 Base' \
    --min-disk 10 15370f09-8cb0-493f-972e-c2359432b7db

# upload image:
glance image-create --name centos7-lbaas-amphora --visibility public \
    --disk-format qcow2 --container-format bare < image.qcow2


# doing a general image using the yum install diskimage-builder package instead of octavia helper script:
disk-image-create -a amd64 -o ubuntu-amd64 vm ubuntu
glance image-create --name ubuntu-xenial --visibility public --disk-format qcow2 \
    --container-format bare < ubuntu-amd64.qcow2 

# TO DELETE AN IMAGE, if protected:
[root@controller-192-168-0-202 ~]# openstack image set --unprotected cf642056-9e26-4330-a49b-97a230e0160b
[root@controller-192-168-0-202 ~]# openstack image delete cf642056-9e26-4330-a49b-97a230e0160b

```

# Octavia madness:
just remember when you create the ssl certificates:
```
    iniset $OCTAVIA_CONF haproxy_amphora client_cert ${OCTAVIA_CERTS_DIR}/client.pem
    iniset $OCTAVIA_CONF haproxy_amphora server_ca ${OCTAVIA_CERTS_DIR}/ca_01.pem
    iniset $OCTAVIA_CONF certificates ca_certificate ${OCTAVIA_CERTS_DIR}/ca_01.pem
    iniset $OCTAVIA_CONF certificates ca_private_key ${OCTAVIA_CERTS_DIR}/private/cakey.pem
    iniset $OCTAVIA_CONF certificates ca_private_key_passphrase foobar
```

I don't think i used this, after updating neutron to use L3 config with vxlan, i also had to make a router and add ports to it:
```
openstack subnet create --network lb-mgmt-net --no-dhcp --gateway 10.96.255.254 --subnet-range 10.96.0.0/16 pod_subnet
openstack router create kuryr-kubernetes
openstack port create --network lb-mgmt-net --fixed-ip ip-address=10.96.255.254 pod_subnet_router
openstack port create --network lb-mgmt-net --fixed-ip ip-address=192.168.0.60 service_subnet_router
openstack router list
openstack router add port e666d592-af14-42ec-9368-64df99c6c2c4 c13a3b5f-6e2f-4a74-948b-4cfeb5766fae
openstack router add port e666d592-af14-42ec-9368-64df99c6c2c4 c615cff0-f738-404b-a701-a2e43d8d020f
```

#Helpful commands
* booting a vm is now better off like:
```bash
openstack server create \
    --image 14fc4a72-eee6-4893-ad3f-d6eccc84b158 \
    --flavor 86ff5992-dad4-4bd5-8a3c-77b5cfceffec \
    --network b41a1d65-d339-40cf-b943-ef0b9ea547cb \
    --key-name=home_auth test-key01.outlaw.cloud

OR when using a ~/.config/openstack/clouds.yaml where os-admin is the name of the _cloud_ ..
openstack server create \
    --image 378ab234-3fed-4d63-9cb2-961044afa626 \
    --flavor f8b9da75-400e-4c8d-a61a-8a6411cef0ef \
    --nic net-id=b41a1d65-d339-40cf-b943-ef0b9ea547cb \
    --key-name home_auth crap01.outlaw.cloud \
    --os-cloud os-admin
```

* Directed build to host
```bash
openstack server create \
    --os-cloud os-admin \
    --image 378ab234-3fed-4d63-9cb2-961044afa626 \
    --flavor f8b9da75-400e-4c8d-a61a-8a6411cef0ef \
    --nic net-id=b41a1d65-d339-40cf-b943-ef0b9ea547cb \
    --key-name home_auth \
    --availability-zone nova:c-192-168-0-214.bropenstack.com crap01.outlaw.cloud 
```

* list vms for all projects and their associated hypervisor
```
openstack server list --os-cloud=os-admin --all-projects --long -c ID -c Host -c Name -c Networks -c "Image Name"
```

# RHEL8 standing up a vm as a rhel8 repo:
make directory for this data:
```
mkdir /opt/rhel8-repo
```
setup apache with a new vhost:
```
[root@rhel8-repo ~]# cat /etc/httpd/conf.d/rhel8-repo.conf
<VirtualHost _default_:80>
DocumentRoot /opt/rhel8-repo
<Directory /opt/rhel8-repo>
    Options Indexes FollowSymLinks
    AllowOverride all
    Require all granted
</Directory>

CustomLog logs/rhel8_repo_access.log combined
ErrorLog logs/rhel8_repo_error.log
</VirtualHost>
```
setup /etc/fstab with new mount:
```
/opt/rhel-8.0-x86_64-dvd.iso	/opt/rhel8-repo	iso9660 	loop 			0 0
mount -a
```
copy the install iso according to fstab:
```
cp/mv file to /opt/rhel-8.0-x86_64-dvd.iso or respective location from fstab
```
for the repo server LOCALLY you can easily add the following repo to install httpd or other packages( baseurl=file ):
```
[root@rhel8-repo ~]# cat /etc/yum.repos.d/rhel8.repo
[rhel8-base]
name=RHEL8 Base Repo
baseurl=file:///opt/rhel8-repo/BaseOS
enabled=1
gpgcheck=1
gpgkey=file:///opt/rhel8-repo/RPM-GPG-KEY-redhat-release

[rhel8-app]
name=RHEL8 AppStream Repo
baseurl=file:///opt/rhel8-repo/AppStream
enabled=1
gpgcheck=1
gpgkey=file:///opt/rhel8-repo/RPM-GPG-KEY-redhat-release
```
For non-local:
```
cat << EOF > /etc/yum.repos.d/rhel8.repo
[rhel8-base]
name=RHEL8 BaseOS
baseurl=http://rhel8-repo.bropenstack.com/BaseOS
gpgcheck=1
enabled=1
gpgkey=http://rhel8-repo.bropenstack.com/RPM-GPG-KEY-redhat-release

[rhel8-app]
name=RHEL8 AppStream
baseurl=http://rhel8-repo.bropenstack.com/AppStream
gpgcheck=1
enabled=1
gpgkey=http://rhel8-repo.bropenstack.com/RPM-GPG-KEY-redhat-release
EOF
```


add the repo to any rhel8 vm that needs more packages than base install, notice baseurl=http in the template:
```
templates/etc/yum.repos.d/rhel8.repo
```

The easiest way to delete compute nodes:
```
[root@controller-192-168-0-202 ~]# nova-manage cell_v2 list_hosts

+-----------+--------------------------------------+-------------------------------------------+
| Cell Name |              Cell UUID               |                  Hostname                 |
+-----------+--------------------------------------+-------------------------------------------+
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-200.bropenstack.com      |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-201.bropenstack.com      |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-203.bropenstack.com      |
|   c0003   | 16552302-370e-4810-901f-87e2ecad0f8f | c-192-168-0-204.c0003.sat.bropenstack.com |
|   c0003   | 16552302-370e-4810-901f-87e2ecad0f8f | c-192-168-0-206.c0003.sat.bropenstack.com |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-207.bropenstack.com      |
+-----------+--------------------------------------+-------------------------------------------+
[root@controller-192-168-0-202 ~]#
[root@controller-192-168-0-202 ~]# nova-manage cell_v2 delete_host --cell_uuid 16552302-370e-4810-901f-87e2ecad0f8f --host c-192-168-0-204.c0003.sat.bropenstack.com
[root@controller-192-168-0-202 ~]# nova-manage cell_v2 list_hosts
+-----------+--------------------------------------+-------------------------------------------+
| Cell Name |              Cell UUID               |                  Hostname                 |
+-----------+--------------------------------------+-------------------------------------------+
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-200.bropenstack.com      |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-201.bropenstack.com      |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-203.bropenstack.com      |
|   c0003   | 16552302-370e-4810-901f-87e2ecad0f8f | c-192-168-0-206.c0003.sat.bropenstack.com |
|   cell1   | 59b72bd1-7782-471b-9957-9435bb5913f6 |      c-192-168-0-207.bropenstack.com      |
+-----------+--------------------------------------+-------------------------------------------+
[root@controller-192-168-0-202 ~]#
```

To ssh to 2960 and ASA fireall, ssh config needs specific cipher:
```
Host datdum48port-switcharoo
	user youknowwww
	StrictHostKeyChecking no
	Ciphers aes256-cbc
	KexAlgorithms +diffie-hellman-group1-sha1
```

centos8 has firewall-cmd fully enabled and selinux enabled.
adjust with:
```
firewall-cmd --add-port=80/tcp --zone=public --permanent
firewall-cmd --reload
vim /etc/sysconfig/selinux
```

# disable nova-compute nodes!!! :D
```
openstack compute service --os-cloud=os-admin list --service nova-compute --long
+----+--------------+------------------------------------------+------------+----------+-------+----------------------------+-----------------+
| ID | Binary       | Host                                     | Zone       | Status   | State | Updated At                 | Disabled Reason |
+----+--------------+------------------------------------------+------------+----------+-------+----------------------------+-----------------+
|  6 | nova-compute | c-192-168-0-200.bropenstack.com          | nova       | enabled  | up    | 2020-01-13T04:37:09.000000 | None            |
|  7 | nova-compute | c-192-168-0-201.bropenstack.com          | sat1-stage | disabled | up    | 2020-01-13T04:37:07.000000 | EOL_HPDL380     |
|  8 | nova-compute | c-192-168-0-203.bropenstack.com          | sat1-stage | disabled | up    | 2020-01-13T04:37:05.000000 | EOL_HPDL380     |
| 12 | nova-compute | c-192-168-0-207.bropenstack.com          | sat1-stage | enabled  | up    | 2020-01-13T04:37:07.000000 | None            |
| 13 | nova-compute | controller-192-168-0-202.bropenstack.com | nova       | enabled  | down  | 2019-06-14T23:56:38.000000 | None            |
| 14 | nova-compute | c-192-168-0-206.bropenstack.com          | nova       | enabled  | up    | 2020-01-13T04:37:06.000000 | None            |
| 15 | nova-compute | c-192-168-0-214.bropenstack.com          | nova       | enabled  | up    | 2020-01-13T04:37:07.000000 | None            |
+----+--------------+------------------------------------------+------------+----------+-------+----------------------------+-----------------+

openstack compute service --os-cloud=os-admin set --disable-reason "EOL_HPDL380" --disable c-192-168-0-201.bropenstack.com nova-compute
openstack compute service --os-cloud=os-admin set --disable-reason "EOL_HPDL380" --disable c-192-168-0-203.bropenstack.com nova-compute

```

# Building OVS:
```
yum install -y @'Development Tools' rpm-build yum-utils
mkdir -p ~/rpmbuild/SOURCES
wget http://openvswitch.org/releases/openvswitch-2.12.0.tar.gz
mv openvswitch-2.12.0.tar.gz rpmbuild/SOURCES/
yum install -y openssl-dev python-devel groff checkpolicy selinux-policy-devel sphinx-build python-twisted-core python-zope-interface libcap-ng-devel unbound unbound-devel python2-oslo-sphinx.noarch python-sphinx.noarch
rpmbuild -bb --nocheck openvswitch-2.12.0/rhel/openvswitch-fedora.spec
rpm -qa |grep openvswitch
ovs-vsctl add-br ovs-br0

modify files as such:
[root@aio-192-168-0-206 ~]# cat /etc/sysconfig/network-scripts/ifcfg-ovs-br0
DEVICE=ovs-br0
DEVICETYPE=ovs
TYPE=OVSBridge
IPADDR=192.168.0.206
NETMASK=255.255.255.0
GATEWAY=192.168.0.1
DNS1=192.168.0.1
BOOTPROTO=static
ONBOOT=yes
NM_CONTROLLED="no"
[root@aio-192-168-0-206 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
TYPE=OVSPort
ONBOOT=yes
DEVICETYPE=ovs
NM_CONTROLLED=no
OVS_BRIDGE=ovs-br0


# reboot box
ovs-vsctl show

# Do the following on compute nodes as well:
yum install -y openstack-neutron-openvswitch.noarch

follow docs for updating configs:
https://docs.openstack.org/ocata/networking-guide/deploy-ovs-provider.html
Update:
/etc/neutron/plugins/ml2/openvswitch_agent.ini

start service:
systemctl start neutron-openvswitch-agent
systemctl enable neutron-openvswitch-agent

now create an openstack network and ensure security groups have acls. (even for ipv6) :))


```

# Scripting image migration:
```
[user@fedora ~]$ cat /isos/save-os-images.sh
#!/bin/bash
source /home/user/openrc.sh
source /home/user/venvs/openstack/bin/activate
# You might need to clean this file up if it has spaces or odd characters in the image name output
openstack image list --os-cloud=pp-admin -c ID -c Name -f value > dl-image-list.txt
cat dl-image-list.txt |while read IMAGE_ID FILE_NAME ; do echo "openstack image save --os-cloud=pp-admin --file $FILE_NAME $IMAGE_ID" ;done
openstack image save --os-cloud=pp-admin --file 2020-04-13.CentOS_8-QUEENS-EOL 7a8da9b2-4444-4d76-9609-f9651b04218c
openstack image save --os-cloud=pp-admin --file 2020-04-13.Debian_10-QUEENS-EOL 1420c17b-a7c0-4271-bfc6-ec1e9a939f0d
openstack image save --os-cloud=pp-admin --file 2020-04-13.RHEL_8.0-QUEENS-EOL 1bf1c737-c9ef-467d-999d-c01f06dcbe90
openstack image save --os-cloud=pp-admin --file 2020-04-13.Server2008R2-QUEENS-EOL e0763654-06c3-46c2-9d7a-80bd891598f9
openstack image save --os-cloud=pp-admin --file 2020-04-13.centos7-lbaas-amphora-QUEENS-EOL 9f2ab94e-4b5f-415d-99f2-ac09678a94b4
openstack image save --os-cloud=pp-admin --file CentOS_7.7 227f3b2e-971c-4105-a919-442306d4afd6
openstack image save --os-cloud=pp-admin --file CentOS_8.1 8acfb9fe-8f5f-45c1-b45a-d715b23e4bf9
openstack image save --os-cloud=pp-admin --file Ubuntu_18.04_LTS c5272a70-6764-4f23-891e-8cdbdf8b7e80
openstack image save --os-cloud=pp-admin --file Ubuntu_20.04_LTS cf4c0c7a-8cc1-4db4-a084-a436b023313c

Now take off the echo and let er rip IN TMUX!!!!!!!


# then in another node in tmux:
openstack image create --os-cloud=dev --disk-format qcow2 --file /isos/2020-04-13.RHEL_8.0-QUEENS-EOL --min-disk 10 --min-ram 1024 --public 2020-04-13.RHEL_8.0-QUEENS-EOL

```

# Base centos 8 and Debian 10 need python installed to run ansible, do the following:
```
# RHEL/CENT
dnf install -y python3
alternatives --set python /usr/bin/python3

# DEBIAN/UBUNTU
apt install -y python3
update-alternatives --install /usr/bin/python python /usr/bin/python3 1
# don't use nano! eww
update-alternatives --config editor
echo 'no set mouse=a' >> ~/.vimrc
echo 'syntax on' >> ~/.vimrc
```

# Enabling OMSA
```
# Install repo and keys
curl -s https://linux.dell.com/repo/hardware/dsu/copygpgkeys.sh | bash
curl -O https://linux.dell.com/repo/hardware/dsu/bootstrap.cgi
bash bootstrap.cgi

# Install software
yum install dell-system-update
dnf install -y srvadmin-all

# Now go to fqdn https and port 1311 for the dell openmanage UI.
```

# LAB Environment notes.
* i need to switch certain ports for PXE: (usually the ports in after Gi0/20 )
```
sw48.oscian.io(config)#do show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/14, Gi0/15, Gi0/16, Gi0/25, Gi0/26, Gi0/27, Gi0/28, Gi0/29, Gi0/30, Gi0/33, Gi0/34, Gi0/35, Gi0/36, Gi0/37, Gi0/38, Gi0/39, Gi0/40, Gi0/41, Gi0/42, Gi0/43, Gi0/44, Gi0/45, Gi0/46
101  MGMT-VLAN101-SAT1                active    Gi0/1, Gi0/2, Gi0/3, Gi0/4, Gi0/5, Gi0/6, Gi0/7, Gi0/8, Gi0/9, Gi0/10, Gi0/11, Gi0/12, Gi0/13, Gi0/17, Gi0/18, Gi0/19, Gi0/21, Gi0/22, Gi0/23, Gi0/24, Gi0/31, Gi0/32, Gi0/47, Gi0/48


# notice ports not in vlan101, they are trunked and need to be updated for PXE booting:
sw48.oscian.io(config-if)#switchport mode access
sw48.oscian.io(config-if)#switchport access vlan 101

# then pxe as normal
# then once complete, put back in trunk
sw48.oscian.io(config-if)#switchport mode trunk
sw48.oscian.io(config-if)#switchport trunk allowed vlan 101

```
| host | port | sw mac neighbor |
|------|------|--------|
| c-192-168-0-200.c0001.sat.bropenstack.com | Gi0/23 | d4be.d9f9.6a75 |
| c-192-168-0-201.c0001.sat.bropenstack.com | Gi0/21 | f04d.a201.fc38 |
| oc-192-168-0-202.c0001.sat.bropenstack.com | Gi0/3 | 782b.cb1d.8a2e |
| c-192-168-0-214.c0001.sat.bropenstack.com | Gi0/24 | d4ae.528d.4e7f |
| c-192-168-0-207.c0001.sat.bropenstack.com | Gi0/20 | d4ae.5292.df36 |
| sn-192-168-0-221.c0001.sat.bropenstack.com  | Gi0/22  | 0024.e85e.c5cb |

# Big lab subnets
| network | subnet | range in use |
|---------|----------------|------|
| c0001.sat public  | 192.168.0.0/20 | 192.168.14.2-192.168.15.254 |
| c0001.preprod-sat public  | 192.168.0.0/20 | 192.168.13.2-192.168.13.254 |

# tshoot computes
* if there's an error in compute logs on centos 8 about parsing the keystone url, reboot, it might be using old python.
