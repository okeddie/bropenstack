- name: Create temp directory for certs
  tempfile:
    state: directory
    path: /etc
    prefix: .ansible_octavia_cert_build.
  register: build_dir

- name: Create subdirectory structure
  file:
    path: "{{ build_dir.path }}/{{ item }}"
    state: directory
  loop:
    - client_ca
    - server_ca

- name: Create server_ca subdirectory structure
  file:
    path: "{{ build_dir.path }}/{{ item }}"
    state: directory
  loop:
    - "server_ca/certs"
    - "server_ca/crl"
    - "server_ca/newcerts"
    - "client_ca/certs"
    - "client_ca/crl"
    - "client_ca/csr"
    - "client_ca/newcerts"

- name: Create server_ca subdirectory structure mode 0700
  file:
    path: "{{ build_dir.path }}/{{ item }}"
    state: directory
    mode: 0700
  loop:
    - "server_ca/private"
    - "client_ca/private"

- name: Create server_ca temp files
  copy:
    dest: "{{ build_dir.path }}/{{ item }}"
    content: ""
  loop:
    - "server_ca/index.txt"
    - "client_ca/index.txt"

- name: Create temp populated files
  copy:
    dest: "{{ build_dir.path }}/{{ item }}"
    content: "1000"
  loop:
    - "server_ca/serial"
    - "client_ca/serial"

- name: Populate temp openssl config
  template:
    src: openssl.cnf
    dest: "{{ build_dir.path }}/openssl.cnf"

- name: Create the server CA key
  openssl_privatekey:
    path: "{{ build_dir.path }}/{{ item }}"
    cipher: aes256
    size: 4096
    passphrase: "{{ master_pass }}"
  loop:
    - "server_ca/private/ca.key.pem"

- name: Create the server CA certificate
  shell: >
    openssl req -config ../openssl.cnf -key private/ca.key.pem 
    -new -x509 -days 7300 -sha256 -extensions v3_ca -out certs/ca.cert.pem
    -subj "/C=US/ST=Texas/O=OpenStack/OU=Octavia/CN=bropenstack.com"
    -passin "pass:{{ master_pass }}"
  args:
    chdir: "{{ build_dir.path }}/server_ca"

- name: Create the client CA key
  openssl_privatekey:
    path: "{{ build_dir.path }}/{{ item }}"
    cipher: aes256
    mode: 0400
    size: 4096
    passphrase: "{{ master_pass }}"
  loop:
    - "client_ca/private/ca.key.pem"

- name: Create the client CA certificate
  shell: >
    openssl req -config ../openssl.cnf 
    -key private/ca.key.pem -new -x509 -days 7300 -sha256 
    -extensions v3_ca -out certs/ca.cert.pem
    -subj "/C=US/ST=Texas/O=OpenStack/OU=Octavia/CN=bropenstack.com"
    -passin "pass:{{ master_pass }}"
  args:
    chdir: "{{ build_dir.path }}/client_ca"

- name: Create the client key
  openssl_privatekey:
    path: "{{ build_dir.path }}/{{ item }}"
    cipher: aes256
    size: 2048
    passphrase: "{{ master_pass }}"
  loop:
    - "client_ca/private/client.key.pem"

- name: Create the client CA certificate
  shell: >
    openssl req -config ../openssl.cnf -new -sha256 
    -key private/client.key.pem -out csr/client.csr.pem
    -subj "/C=US/ST=Texas/O=OpenStack/OU=Octavia/CN=bropenstack.com"
    -passin "pass:{{ master_pass }}"
  args:
    chdir: "{{ build_dir.path }}/client_ca"

- name: Sign the client certificate request
  shell: >
    openssl ca -config ../openssl.cnf -extensions usr_cert
    -days 7300 -notext -md sha256 -in csr/client.csr.pem
    -out certs/client.cert.pem -batch
    -passin "pass:{{ master_pass }}"
  args:
    chdir: "{{ build_dir.path }}/client_ca"

- name: Create a decrypted key for future pem
  shell: >
    openssl rsa -in private/client.key.pem
    -out private/client.cert-and-key.pem
    -passin "pass:{{ master_pass }}"
  args:
    chdir: "{{ build_dir.path }}/client_ca"

- name: Create a concatenated client certificate and key file
  shell: >
    cat certs/client.cert.pem >> private/client.cert-and-key.pem
  args:
    chdir: "{{ build_dir.path }}/client_ca"

- name: Create permanent octavia certs directory
  file:
    path: /etc/octavia/certs
    mode: 0700
    owner: octavia
    group: octavia
    state: directory

- name: Copy Server CA key file to /etc/octavia/certs/
  copy:
    remote_src: true
    src:  "{{ build_dir.path }}/server_ca/private/ca.key.pem"
    dest: "/etc/octavia/certs/server_ca.key.pem"
    mode: 0700
    owner: octavia
    group: octavia

- name: Copy Server CA cert to /etc/octavia/certs/
  copy:
    owner: octavia
    group: octavia
    remote_src: true
    src: "{{ build_dir.path }}/server_ca/certs/ca.cert.pem"
    dest: "/etc/octavia/certs/server_ca.cert.pem"

- name: Copy Client CA certs to /etc/octavia/certs/
  copy:
    owner: octavia
    group: octavia
    remote_src: true
    src: "{{ build_dir.path }}/client_ca/certs/ca.cert.pem"
    dest: "/etc/octavia/certs/client_ca.cert.pem"

- name: Copy Server cert and key to /etc/octavia/certs/
  copy:
    owner: octavia
    group: octavia
    mode: 0700
    remote_src: true
    src: "{{ build_dir.path }}/client_ca/private/client.cert-and-key.pem"
    dest: "/etc/octavia/certs/client.cert-and-key.pem"

- name: pauser
  pause: seconds=90

- name: Remove temp dir
  file:
    state: absent
    path: "{{ build_dir.path }}"
