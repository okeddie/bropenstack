LAUNCH_INSTANCE_DEFAULTS = {
    'config_drive': False,
    'create_volume': False,
    'hide_create_volume': True,
    'disable_image': False,
    'disable_instance_snapshot': False,
    'disable_volume': True,
    'disable_volume_snapshot': True,
    'enable_scheduler_hints': False,
}
