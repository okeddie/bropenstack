#!/bin/bash
echo "Checking requirements"
dnf install -y NetworkManager-ovs

# clean old files
echo "Removing old files"
find /etc/NetworkManager/system-connections -type f -delete
echo "OK"

echo "Deleting network connections..."
nmcli --fields UUID,TIMESTAMP-REAL con show
echo "OK"

for i in $(nmcli -g UUID con show)
do nmcli con delete $i
done

# need to use nmcli to create the ovs bridge:
# maybe no use this br0 config
#nmcli con add type ovs-bridge conn.interface br0 con-name br0
#nmcli con add type ovs-port conn.interface br0 master br0 con-name ovs-port-br0
#nmcli con add type ovs-interface slave-type ovs-port con.interface br0 master ovs-port-br0 con-name ovs-if-br0 connection.autoconnect no ipv4.never-default true
#nmcli con add type ovs-port conn.interface eth0 master br0 con-name ovs-port-eth0
#nmcli con add type ethernet conn.interface eth0 master ovs-port-eth0 con-name ovs-if-eth0

# however, yes use eth1's config
nmcli con add type ovs-bridge conn.interface ovs-br1 con-name ovs-br1
nmcli con add type ovs-port conn.interface ovs-br1 master ovs-br1 con-name ovs-port-br1
nmcli con add type ovs-interface slave-type ovs-port con.interface ovs-br1 master ovs-port-br1 con-name ovs-if-br1 ipv4.address {{ node_ip }}/24 ipv4.method manual ipv4.gateway 192.168.0.1 ipv4.dns 192.168.0.1 ipv4.ignore-auto-dns yes ipv4.never-default false
nmcli con add type ovs-port conn.interface eth1 master ovs-br1 con-name ovs-port-eth1
nmcli con add type ethernet conn.interface eth1 master ovs-port-eth1 con-name ovs-if-eth1

# Proper moc for br0 ovs.
# nmcli con add type ovs-bridge conn.interface ovs-br0 con-name ovs-br0
# nmcli con add type ovs-port conn.interface eth0 master ovs-br0 con-name ovs-port-eth0
# nmcli con add type ethernet conn.interface eth0 master ovs-port-eth0 con-name eth0
# If this doesn't create your bridge and ports in ovs, then proceed:
# ovs-vsctl add-br ovs-br0
# ovs-vsctl add-port ovs-br0 eth0
