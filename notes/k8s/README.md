# kubernetes bootstrapping notes
1. when you run kubeadm init, the pod cidr is possibly left off
  * we probably want to try ```--cloud-provider=external``` on init
  * you can add --pod-cidr=10.244.0.0/16 to kubeadm init or
  * you can patch running kubemaster and reboot with  
  ```bash
kubectl patch node $(hostname) -p '{"spec":{"podCIDR":"10.244.0.0/16"}}'
  ```
2. The pod cidr is taken from the flannel install config
```bash
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

which has:
  net-conf.json: |
    {
      "Network": "10.244.0.0/16",
      "Backend": {
        "Type": "vxlan"
      }
    }
```

3. now we need to apply openstack rbac
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-roles.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml
```

4. then we can add credentials to cloud.conf
```
vim /etc/kubernetes/cloud.conf

[Global]
auth-url=http://oc-192-168-0-202.c0001.sat.bropenstack.com:5000/v3/
username=admin
password=REDACTED
region=sat
tenant-id=92bb118f992c493fb402da01b1b70067
domain-id=default

[LoadBalancer]
use-octavia=true
subnet-id=9c0d7bef-2962-4599-89a6-a85e5a6170c2
internal-lb=false
cascade-delete=true
floating-network-id=caebd815-8409-4739-82d9-e14ff91ae41b

[BlockStorage]
```

5. then we can create the secret from file
```
kubectl create secret -n kube-system generic cloud-config --from-file=cloud.conf
```

6. then i found that the cloud setup adds a taint, i remove it, then bounce the openstack-cloud-controller-manager pod
```
[eddie.vasquez@air ~]$ kubectl taint nodes k01 node.cloudprovider.kubernetes.io/uninitialized:NoSchedule-
node/k01 untainted
```


# Testing openstack integration
using the bitnami helm chart, you can easily make a test wordpress release with loadbalancer and no pvc's.
[oscian-front-example](https://gitlab.com/okeddie/oscian-front)


# Upgrading kubeadm
It so happened that i had upgraded packages of kubeadm to 1.23 but that didn't support kubeadm join the way i needed.  
Following https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/  i was able to walk the upgrade of the control plan using:
```
apt-cache madison kubeadm
apt-get install -y kubeadm=1.21.8-00

kubeadm upgrade plan
# this gave output for upgrading
kubeadm upgrade apply v1.21.8

# rinse and repeat until you have the right version compatible for `join`
```

# Node being deleted upon join
This odd case where the nodes hostname didn't match what was in openstack caused the node to be rejected?  
once i set the ubuntu hostname to exactly what fqdn and name field from openstack was, it was added.
```
kubectl get events -w
0s          Normal    NodeHasSufficientPID                               node/k04   Node k04 status is now: NodeHasSufficientPID
0s          Normal    NodeAllocatableEnforced                            node/k04   Updated Node Allocatable limit across pods
0s          Normal    Starting                                           node/k04   Starting kubelet.
0s          Normal    NodeHasSufficientMemory                            node/k04   Node k04 status is now: NodeHasSufficientMemory
0s          Normal    NodeHasNoDiskPressure                              node/k04   Node k04 status is now: NodeHasNoDiskPressure
0s          Normal    NodeHasSufficientPID                               node/k04   Node k04 status is now: NodeHasSufficientPID
0s          Normal    NodeAllocatableEnforced                            node/k04   Updated Node Allocatable limit across pods
0s          Normal    DeletingNode                                       node/k04   Deleting node k04 because it does not exist in the cloud provider
0s          Normal    Starting                                           node/k04.sat.oscian.io   Starting kubelet.
0s          Normal    NodeHasSufficientMemory                            node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasSufficientMemory
0s          Normal    NodeHasNoDiskPressure                              node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasNoDiskPressure
0s          Normal    NodeHasSufficientPID                               node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasSufficientPID
0s          Normal    NodeHasSufficientMemory                            node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasSufficientMemory
0s          Normal    NodeHasNoDiskPressure                              node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasNoDiskPressure
0s          Normal    NodeHasSufficientPID                               node/k04.sat.oscian.io   Node k04.sat.oscian.io status is now: NodeHasSufficientPID
0s          Normal    NodeAllocatableEnforced                            node/k04.sat.oscian.io   Updated Node Allocatable limit across pods

[eddie.vasquez@air forks]$ kubectl get nodes -o wide
NAME                STATUS   ROLES                  AGE     VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
k01                 Ready    control-plane,master   3h21m   v1.23.1   192.168.15.247   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
k04.sat.oscian.io   Ready    <none>                 62s     v1.23.1   192.168.15.5     <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
[eddie.vasquez@air forks]$ kubectl get nodes -o wide
NAME                STATUS   ROLES                  AGE     VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
k01                 Ready    control-plane,master   3h24m   v1.23.1   192.168.15.247   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
k02.sat.oscian.io   Ready    <none>                 51s     v1.23.1   192.168.15.253   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
k03.sat.oscian.io   Ready    <none>                 55s     v1.23.1   192.168.15.131   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
k04.sat.oscian.io   Ready    <none>                 4m32s   v1.23.1   192.168.15.5     <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
k05.sat.oscian.io   Ready    <none>                 34s     v1.23.1   192.168.15.61    <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.7
```

# Installed kube
* created /etc/kubernetes/cloud.conf
* added option ```--cloud-provider=external``` to `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf`
* ran the kubeinit as normal
```
root@k01:~# kubeadm init --control-plane-endpoint=k01.sat.oscian.io --pod-network-cidr=10.244.0.0/16
[init] Using Kubernetes version: v1.23.4
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [k01 k01.sat.oscian.io kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 192.168.15.247]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [k01 localhost] and IPs [192.168.15.247 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [k01 localhost] and IPs [192.168.15.247 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 20.147023 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.23" in namespace kube-system with the configuration for the kubelets in the cluster
NOTE: The "kubelet-config-1.23" naming of the kubelet ConfigMap is deprecated. Once the UnversionedKubeletConfigMap feature gate graduates to Beta the default name will become just "kubelet-config". Kubeadm upgrade will handle this transition transparently.
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node k01 as control-plane by adding the labels: [node-role.kubernetes.io/master(deprecated) node-role.kubernetes.io/control-plane node.kubernetes.io/exclude-from-external-load-balancers]
[mark-control-plane] Marking the node k01 as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: vzk62i.cvmetfj2e08m080r
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join k01.sat.oscian.io:6443 --token vzk62i.cvmetfj2e08m080r \
	--discovery-token-ca-cert-hash sha256:a9c93c2e41ac87022328549bedb09edf28411483f7b7c4cf517ea77a2b73909c \
	--control-plane

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join k01.sat.oscian.io:6443 --token vzk62i.cvmetfj2e08m080r \
	--discovery-token-ca-cert-hash sha256:a9c93c2e41ac87022328549bedb09edf28411483f7b7c4cf517ea77a2b73909c
```

* now install flannel
```
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
```

* now installed other nodes
```
kubeadm join k01.sat.oscian.io:6443 --token vzk62i.cvmetfj2e08m080r \
	--discovery-token-ca-cert-hash sha256:a9c93c2e41ac87022328549bedb09edf28411483f7b7c4cf517ea77a2b73909c
```

* now we have nodes added to cluster
```
root@k01:~# kubectl get nodes
NAME   STATUS     ROLES                  AGE     VERSION
k01    Ready      control-plane,master   7m30s   v1.23.3
k02    NotReady   <none>                 9s      v1.23.3
k03    NotReady   <none>                 8s      v1.23.3
k04    NotReady   <none>                 14s     v1.23.3
k05    NotReady   <none>                 12s     v1.23.3
```

* after waiting a few minutes we now have nodes ready
```
root@k01:~# kubectl get nodes
NAME   STATUS   ROLES                  AGE     VERSION
k01    Ready    control-plane,master   8m19s   v1.23.3
k02    Ready    <none>                 58s     v1.23.3
k03    Ready    <none>                 57s     v1.23.3
k04    Ready    <none>                 63s     v1.23.3
k05    Ready    <none>                 61s     v1.23.3
```

* now since we have made cloud controller in config we need to install that:
```
we initially got error in testing:
  Warning  FailedScheduling  20s   default-scheduler  0/5 nodes are available: 1 node(s) had taint {node-role.kubernetes.io/master: }, that the pod didn't tolerate, 4 node(s) had taint {node.cloudprovider.kubernetes.io/uninitialized: true}, that the pod didn't tolerate.

kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-roles.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml


root@k01:~# kubectl create secret -n kube-system generic cloud-config --from-file=/etc/kubernetes/cloud.conf
secret/cloud-config created


[eddie.vasquez@air ~]$ kubectl get pods -n kube-system
NAME                                       READY   STATUS    RESTARTS   AGE
coredns-64897985d-5vsr9                    0/1     Pending   0          14m
coredns-64897985d-wzzck                    0/1     Pending   0          14m
etcd-k01                                   1/1     Running   0          14m
kube-apiserver-k01                         1/1     Running   0          14m
kube-controller-manager-k01                1/1     Running   0          14m
kube-flannel-ds-2ml7z                      1/1     Running   0          7m33s
kube-flannel-ds-5f7cm                      1/1     Running   0          7m37s
kube-flannel-ds-dkdnl                      1/1     Running   0          9m58s
kube-flannel-ds-lh6gq                      1/1     Running   0          7m39s
kube-flannel-ds-z2qv7                      1/1     Running   0          7m34s
kube-proxy-6ckgs                           1/1     Running   0          7m39s
kube-proxy-7x4cx                           1/1     Running   0          7m34s
kube-proxy-c86wq                           1/1     Running   0          14m
kube-proxy-s96mg                           1/1     Running   0          7m37s
kube-proxy-vtf9s                           1/1     Running   0          7m33s
kube-scheduler-k01                         1/1     Running   0          14m
openstack-cloud-controller-manager-kb7wx   1/1     Running   0          103s
```

* DNS pods are broken seen above
```
# so we need to untaint k01-5 since the openstack cloud controller is running
[eddie.vasquez@air ~]$ for i in {1..5} ; do kubectl taint nodes k0$i node.cloudprovider.kubernetes.io/uninitialized:NoSchedule- ;done
error: taint "node.cloudprovider.kubernetes.io/uninitialized:NoSchedule" not found
node/k02 untainted
node/k03 untainted
node/k04 untainted
node/k05 untainted
```

* finally let's test! AND IT WORKS!
```
kubectl run -it --rm --restart=Never -n default eddiev01-test --image=ubuntu bash

DEBIAN_FRONTEND=noninteractive apt update && DEBIAN_FRONTEND=noninteractive apt install -y wget iputils-ping dnsutils curl telnet
```

* reboot everything for good measure.
