#!/bin/bash
LB_UUID=$1
if [ -z "$LB_UUID" ]; then
	openstack loadbalancer list
	echo -n "Please select an LB_ID: " ; read LB_UUID
	echo "Gathering info. This might take a minute."
else
	echo "Attempting delete workflow for $LB_UUID"
fi

POOL_ID=$(openstack loadbalancer pool list --loadbalancer ${LB_UUID} |awk '/HTTP/{print $2}')
LISTENER_ID=$(openstack loadbalancer listener list --loadbalancer ${LB_UUID} |awk '/HTTP/{print $2}')

if [ -n "$POOL_ID" ]; then
	echo "Deleting Pool $POOL_ID..."
	openstack loadbalancer pool delete $POOL_ID
	if [ $? == "0" ]; then
		echo "Deleted."
	else
		echo "Delete was not successful."
		exit 1
	fi
fi

sleep 5
if [ -n "$LISTENER_ID" ]; then
	echo "Deleting Listener $LISTENER_ID..."
	openstack loadbalancer listener delete $LISTENER_ID
	if [ $? == "0" ]; then 
        	echo "Deleted."
	else
        	echo "Delete was not successful."
        	exit 1
	fi
fi

sleep 5
echo "Deleting LB $LB_UUID"
openstack loadbalancer delete $LB_UUID
if [ $? == "0" ]; then 
        echo "Deleted."
else
        echo "Delete was not successful."
        exit 1
fi
