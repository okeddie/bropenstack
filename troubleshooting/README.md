# problem - resize error:
```
ERROR oslo_messaging.rpc.server     if ret == -1: raise libvirtError ('virDomainCreateWithFlags() failed', dom=self)
ERROR oslo_messaging.rpc.server libvirt.libvirtError: Cannot access storage file '$PATHTOINSTANCE' (as uid:107, gid:107): Permission denied
```
* this is because libvirt is running as qemu user and nova user/group owns all these files for resizes and migrations
## solution - ensure /etc/libvirt/qemu.conf has lines explicit for nova user/group.
```
user = nova
group = nova
```


# PROBLEM - the dreaded error about kvm:
```
libvirt.libvirtError: unsupported configuration: Emulator '/usr/bin/qemu-system-x86_64' does not support virt type 'kvm'
```

# SOLUTION - through much RAGE with my HPz440, this turned out to be BIOS was reset and virtualization was disabled.
  * Fix virt in BIOS, hit ESC at boot and F10 or simply navigate into HP BIOS in this hardware specific situation.  
  * sad note i fixed this once before but didn't document it, but i had way more problems this time and literally needed
    to swap out the graphics card to get the BIOS screen to show up. it's sooo olllld.
    ```
    root@c-192-168-12-2:~# lspci -vv |grep -i gigabyte
	Subsystem: Gigabyte Technology Co., Ltd GF108 [GeForce GT 420]
	Subsystem: Gigabyte Technology Co., Ltd GF108 High Definition Audio Controller
    ```
  * `:rage_doom_go_bazzerk:`
  * i unfortunately deleted the previous instances in nova-db via the UI so that was dataloss. i should have just fixed  
    virtualization for intel in the BIOS. ended up just running ```terraform apply -auto-approve``` to replenish them.
