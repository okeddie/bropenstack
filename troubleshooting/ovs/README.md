# Recent as of 11-5-2021
* When setting up ovs-br0, it's ok to have the interface not configured in netplan but you absolutely need an unused interface for it.
* Problem was i had 1 nic, br0 and ovs-br0 were both on eth0 nic, but ovs-vsctl would complain device was busy and couldn't use it.
* Once i added a 2nd nic and left that as br1, things were happy in ovs to utilize the bridge.
```
root@aio-192-168-12-1:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether 3c:7c:3f:c2:0a:f9 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::3e7c:3fff:fec2:af9/64 scope link
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master br1 state UP group default qlen 1000
    link/ether 00:13:3b:4b:7f:12 brd ff:ff:ff:ff:ff:ff
4: br1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:13:3b:4b:7f:12 brd ff:ff:ff:ff:ff:ff
    inet 192.168.12.1/20 brd 192.168.15.255 scope global br1
       valid_lft forever preferred_lft forever
    inet6 fe80::4080:99ff:feb2:b391/64 scope link
       valid_lft forever preferred_lft forever
5: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 12:36:93:d8:ca:80 brd ff:ff:ff:ff:ff:ff
6: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 72:46:73:ca:bd:45 brd ff:ff:ff:ff:ff:ff
7: ovs-br0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 3c:7c:3f:c2:0a:f9 brd ff:ff:ff:ff:ff:ff
8: qbrb9a6966d-c0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 6a:e8:12:b6:d3:eb brd ff:ff:ff:ff:ff:ff
9: qvob9a6966d-c0@qvbb9a6966d-c0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master ovs-system state UP group default qlen 1000
    link/ether 0a:76:27:b6:a4:48 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::876:27ff:feb6:a448/64 scope link
       valid_lft forever preferred_lft forever
10: qvbb9a6966d-c0@qvob9a6966d-c0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master qbrb9a6966d-c0 state UP group default qlen 1000
    link/ether 6a:e8:12:b6:d3:eb brd ff:ff:ff:ff:ff:ff
    inet6 fe80::68e8:12ff:feb6:d3eb/64 scope link
       valid_lft forever preferred_lft forever
11: tapb9a6966d-c0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master qbrb9a6966d-c0 state UNKNOWN group default qlen 1000
    link/ether fe:16:3e:0a:ec:3c brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc16:3eff:fe0a:ec3c/64 scope link
       valid_lft forever preferred_lft forever
root@aio-192-168-12-1:~# cat /etc/netplan/01-netcfg.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      set-name: eth0
      match:
        macaddress: 3c:7c:3f:c2:0a:f9
    eth1:
      dhcp4: no
      set-name: eth1
      match:
        macaddress: 00:13:3b:4b:7f:12
  bridges:
    br1:
      dhcp4: no
      interfaces:
        - eth1
      addresses:
        - 192.168.12.1/20
      gateway4: 192.168.0.1
      nameservers:
        addresses:
          - 192.168.0.1
root@aio-192-168-12-1:~# ovs-vsctl show
0b52f0a1-6e67-4134-8b4f-9c73fe3106f8
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge ovs-br0
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port ovs-br0
            Interface ovs-br0
                type: internal
        Port eth0
            Interface eth0
        Port phy-ovs-br0
            Interface phy-ovs-br0
                type: patch
                options: {peer=int-ovs-br0}
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-int
            Interface br-int
                type: internal
        Port int-ovs-br0
            Interface int-ovs-br0
                type: patch
                options: {peer=phy-ovs-br0}
        Port qvob9a6966d-c0
            tag: 1
            Interface qvob9a6966d-c0
    ovs_version: "2.15.0"
```

# Notes on ovs bridge setups
- when using neutron-openvswitch-agent to handle plugging vifs
* Mapping will follow as `/etc/neutron/plugins/ml2/openvswitch_agent.ini`
```
bridge_mappings = private:ovs-br0,public:ovs-br1
```
* Therefore, on ubuntu, make netplan look like this
```
root@c-192-168-0-95:~# cat /etc/netplan/01-netcfg.yaml
# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      set-name: eth0
      match:
        macaddress: 90:2b:34:98:ef:86
    eth1:
      dhcp4: no
      set-name: eth1
      match:
        macaddress: 00:07:e9:02:08:45
      addresses:
        - 192.168.0.95/24
      gateway4: 192.168.0.1
      nameservers:
        addresses:
          - 192.168.0.1
```
* And CentOS make look like this
```
[root@aio-192-168-0-206 ~]# cat /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
TYPE=OVSPort
ONBOOT=yes
DEVICETYPE=ovs
NM_CONTROLLED=no
OVS_BRIDGE=ovs-br0
[root@aio-192-168-0-206 ~]# cat /etc/sysconfig/network-scripts/ifcfg-ovs-br0
DEVICE=ovs-br0
DEVICETYPE=ovs
TYPE=OVSBridge
IPADDR=192.168.0.206
NETMASK=255.255.255.0
GATEWAY=192.168.0.1
DNS1=192.168.0.1
BOOTPROTO=static
ONBOOT=yes
NM_CONTROLLED="no"
[root@aio-192-168-0-206 ~]# cat /etc/sysconfig/network-scripts/ifcfg-ovs-br-ex
DEVICE=ovs-br-ex
DEVICETYPE=ovs
TYPE=OVSBridge
BOOTPROTO=static
ONBOOT=yes
NM_CONTROLLED="no"
```

* Then it might be necessary to create the bridges in ovs like
```
ovs-vsctl add-br ovs-br0
ovs-vsctl add-port ovs-br0 eth0

# example for later
ovs-vsctl add-port ovs-bridge-br0 eth0
ovs-vsctl add-port ovs-bridge-br1 eth1

# This made connection work:
[root@oc ~]# ovs-vsctl show
    Bridge "br0"
        Port "eth0"
            Interface "eth0"
                type: system
        Port "br0"
            Interface "br0"
                type: internal
    Bridge "ovs-bridge-br1"
        Port "eth1"
            Interface "eth1"
        Port "ovs-bridge-br1"
            Interface "ovs-bridge-br1"
                type: internal
    Bridge "ovs-bridge-br0"
        Port "ovs-bridge-br0"
            Interface "ovs-bridge-br0"
                type: internal
    ovs_version: "2.12.0"
```

# Building openvswitch setup on centos 8
- ENSURE YOU HAVE NetowrkManager-ovs installed before making any changes here.
- manually create the interfaces using nmcli because ansible's nmcli isn't working with python3 + centos8

```
# need to use nmcli to create the ovs bridge:
nmcli con add type ovs-bridge conn.interface br0 con-name br0
nmcli con add type ovs-port conn.interface br0 master br0 con-name ovs-port-br0
nmcli con add type ovs-interface slave-type ovs-port con.interface br0 master ovs-port-br0 con-name ovs-if-br0 ipv4.address 192.168.0.88/24 ipv4.method manual ipv4.gateway 192.168.0.1

# creating the bridge won't automatically create an interface for you. The two additional commands above get us an actual interface named br0

# now we need to add eth0 to the bridge
nmcli con add type ovs-port conn.interface eth0 master br0 con-name ovs-port-eth0
nmcli con add type ethernet conn.interface eth0 master ovs-port-eth0 con-name ovs-if-eth0

```
