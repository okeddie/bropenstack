#!/bin/bash
# Usage: ./create_ini.sh example_ini_input.txt 
# where example_ini_input.txt is a 2 column list with option value pair.

INPUT_FILE=$1
while read i j ;do echo "    - { option: '$i', value: '$j' }" ;done < $INPUT_FILE
# end.